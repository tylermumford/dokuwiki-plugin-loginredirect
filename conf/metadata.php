<?php
/*
 *
 */
$meta['url']        = array('string');   // location for login redirects
$meta['logout_url'] = array('string');   // location for logout redirects
$meta['return_key'] = array('string');   // query string variable to supply return URI
